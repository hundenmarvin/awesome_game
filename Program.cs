﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwesomGame
{
    class Program
    {
        //some properties and constants
        public static string input;

        public static int width  = 32;
        public static int height = 21;

        //static bool firstFrame = true;

        public static string[,] render = new string[height, width];

  

        static void Main(string[] args)
        {
            Debug.counter = 0;
            //if (Debug.counter == 1)
            //    Console.WriteLine();
            //Debug.counter++;

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;

            Console.SetWindowSize(width, height);

            Hero.status = "start";

            while (true)
            {

                if (Hero.status == "start")
                    StartScreen();

                InitGame();
                TheFrameLoop();

                if (input == "esc")
                    break;
                if (Hero.status == "dead")
                    DeathScreen();
                if (Hero.status == "won")
                    WinningScreen();
            }
            
            
            
        }
        //some methods

        static void StartScreen()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Space Station Pheta");
            Console.WriteLine();
            Console.WriteLine("Use WASD-keys");
            System.Threading.Thread.Sleep(6000);
            //Console.WriteLine();
            //Console.WriteLine("Press space to continue");
        }

        static void DeathScreen()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("You died!");
            Hero.status = "start";
            System.Threading.Thread.Sleep(4000);
            //Console.WriteLine();
            //Console.WriteLine("Press space to continue");
            //Console.ReadKey();
        }

        static void WinningScreen()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("You won!");
            Hero.status = "start";
            System.Threading.Thread.Sleep(4000);
            //Console.WriteLine();
            //Console.WriteLine("Press space to continue");
            //Console.ReadKey();
        }

        static void InitGame()
        {
            

            //just action - or equivalent of action
            Hero.positionY = Level.startY;
            Hero.positionX = Level.startX;

            Hero.status = "standing";

            Hero.step = 0;

            Hero.direction = "right";


            DrawBackground();
            DrawHero();
            Render();
        }


        static void TheFrameLoop()
        {
            while (true)//the frame loop====================================================
            {
                //var timePassedSinceLastLoop = CalculateTimeDelta();
                //var time = DateTime.Now;
                //Console.WriteLine(time);

                HeroContact();
                HeroConsequence();
                if (Hero.status == "standing" || Hero.status == "climbing")
                    HandleUserInput();
                if (input == "esc")
                    break;
                if (Hero.status == "dead")
                    break;
                if (Hero.status == "won")
                    break;
                HeroAction();

                //ChangeWorld();//(timePassedSinceLastLoop);

                DrawBackground();
                DrawHero();
                Render();
                input = "nothing";
                System.Threading.Thread.Sleep(50);


            }//=============================================================================
        }





        static void HandleUserInput()
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey(true);//it pauses the game
            switch (keyInfo.Key)
            {
                case ConsoleKey.Escape:
                    input = "esc";
                    break;

                case ConsoleKey.W:
                    input = "up";
                    break;

                case ConsoleKey.A:
                    input = "left";
                    break;

                case ConsoleKey.S:
                    input = "down";
                    break;

                case ConsoleKey.D:
                    input = "right";
                    break;
            }
        }

        static void DrawBackground()
        {
            Console.SetCursorPosition(0, 0);//-----------------------------why
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    render[y, x] = Level.map[y][x].ToString();
                }
            }
        }

        static void HeroContact()
        {
            //consequence
            //list blocks in contact with hero
            Hero.at = Level.map[Hero.positionY][Hero.positionX].ToString();

            try
            {
                Hero.below = Level.map[Hero.positionY + 1][Hero.positionX].ToString();
            }
            catch (IndexOutOfRangeException)
            {
                Hero.below = "Z";
            }

            try
            {
                Hero.above = Level.map[Hero.positionY - 1][Hero.positionX].ToString();
            }
            catch (IndexOutOfRangeException)
            {
                Hero.above = "Z";
            }

            try
            {
                Hero.leftSide = Level.map[Hero.positionY][Hero.positionX - 1].ToString();
            }
            catch (IndexOutOfRangeException)
            {
                Hero.leftSide = "Z";
            }

            try
            {
                Hero.rightSide = Level.map[Hero.positionY][Hero.positionX + 1].ToString();
            }
            catch (IndexOutOfRangeException)
            {
                Hero.rightSide = "Z";
            }
        }


        static void HeroConsequence()
        {
            //consequence - next part
            if (Hero.status == "jumping")
            {
                if (Hero.step <= 2)//if hero is in the sideway part of his jump-curve
                {
                    if (Hero.direction == "right" && Hero.rightSide == " ")
                    {
                        Hero.positionX = Hero.positionX + 1;
                        Hero.step++;
                    }
                    else if (Hero.direction == "left" && Hero.leftSide == " ")
                    {
                        Hero.positionX = Hero.positionX - 1;
                        Hero.step++;
                    }
                    else
                    {
                        Hero.step++;
                    }
                }
                else if (Hero.step > 2)//if hero is going from jumping to falling
                {
                    if (Hero.below == " ")
                    {
                        Hero.positionY = Hero.positionY + 1;
                        Hero.status = "falling";
                        Hero.step = 1;
                    }
                    else
                    {
                        Hero.status = "standing";
                    }
                }
            }
            else if (Hero.status == "falling")
            {
                if (Hero.step <= 3)//if hero has not fallen to far to die yet
                {
                    if (Hero.below == " ")
                    {
                        Hero.positionY = Hero.positionY + 1;
                        Hero.step++;
                    }
                    else
                    {
                        Hero.status = "standing";
                    }
                }
                else//if hero is about to fall far enough to meet death
                {
                    if (Hero.below == " ")
                    {
                        Hero.positionY = Hero.positionY + 1;
                        Hero.status = "fallingFast";
                    }
                    else
                    {
                        Hero.status = "daying";//is he really gonna die here?
                        Hero.step = 1;
                    }
                }
            }
            else if (Hero.status == "fallingFast")//if hero has fallen far enough to meet death
            {
                if (Hero.below == " ")
                {
                    Hero.positionY = Hero.positionY + 1;
                }
                else
                {
                    Hero.status = "daying";
                    Hero.step = 1;
                }
            }
            else if (Hero.status == "daying")
            {
                Hero.step++;
                if (Hero.step >= 20)
                {
                    Hero.status = "dead";
                }
            }
            else if (Hero.status == "standing")
            {
                if (Hero.at == "H")
                {
                    Hero.status = "climbing";
                }
                else if (Hero.below == " ")
                {
                    Hero.positionY = Hero.positionY + 1;
                    Hero.status = "falling";
                    Hero.step = 3;//is it the right amount of steps?
                }
                else if (Hero.at == "D")
                {
                    Hero.status = "won";
                }
            }
            else if (Hero.status == "climbing" && Hero.at != "H")
            {
                //if (at != "H")
                //{
                if (Hero.below == " ")
                {
                    Hero.positionY = Hero.positionY + 1;
                    Hero.status = "falling";
                    Hero.step = 3;//is it the right amount of steps?
                }
                else
                {
                    Hero.status = "standing";
                }
                //}
            }
        }

        static void HeroAction()
        {
            //action
            if (Hero.status == "standing")//if hero responds to player-action
            {
                switch (input)
                {
                    case "right":
                        if (Hero.rightSide == " " || Hero.rightSide == "H" || Hero.rightSide == "D")
                        {
                            Hero.direction = "right";
                            Hero.positionX = Hero.positionX + 1;
                        }
                        break;

                    case "left":
                        if (Hero.leftSide == " " || Hero.leftSide == "H" || Hero.leftSide == "D")
                        {
                            Hero.direction = "left";
                            Hero.positionX = Hero.positionX - 1;
                        }
                        break;

                    case "up":
                        if (Hero.above == " ")
                        {
                            Hero.status = "jumping";
                            Hero.step = 1;
                            Hero.positionY = Hero.positionY - 1;

                        }
                        break;

                    case "down":
                        if (Hero.below == "H")
                        {
                            //Hero.status = "climbing";
                            //Hero.step = 1;
                            Hero.positionY = Hero.positionY + 1;

                        }
                        break;
                }
            }
            else if (Hero.status == "climbing")//if hero responds to player-action
            {
                    
                    
                switch (input)
                {
                    case "right":
                        if (Hero.rightSide == " " || Hero.rightSide == "H")
                        {
                            Hero.direction = "right";
                            Hero.positionX = Hero.positionX + 1;
                        }
                        break;

                    case "left":
                        if (Hero.leftSide == " " || Hero.leftSide == "H")
                        {
                            Hero.direction = "left";
                            Hero.positionX = Hero.positionX - 1;
                        }
                        break;

                    case "up":
                        if (Hero.above == " " || Hero.above == "H")
                        {
                                
                            Hero.positionY = Hero.positionY - 1;
                        }
                        break;

                    case "down":
                        if (Hero.below == " " || Hero.below == "H")
                        {
                            Hero.positionY = Hero.positionY + 1;
                        }
                        break;
                }
                    
            }
        }


        static void DrawHero()
        {
            //draw hero to render-buffer
            if (Hero.direction == "right")
            {
                render[Hero.positionY, Hero.positionX] = ">";
            }
            else
            {
                render[Hero.positionY, Hero.positionX] = "<";
            }
        }


            //what happens to the player - consequence
                //if on item than get item
                //if above ground than standing
                //if above nothing than falling
            //what does the player do - action
                //if right
                    //if standing
                        //if open to the right
                            //move player 1 step right

        

        static void Render()
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            for (int y = 0; y < height; y ++)
            {
                for (int x = 0; x < width; x ++)
                {
                    if (render[y, x] == "Z")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    else if (render[y, x] == "<" || render[y, x] == ">")
                    {
                        {
                            Console.ForegroundColor = ConsoleColor.Blue;
                            if (Hero.status == "daying" && Hero.step %2 == 0)
                                Console.ForegroundColor = ConsoleColor.Yellow;
                        }
                    }
                    else if (render[y, x] == "D")
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                    }
                    Console.Write(render[y, x]);
                }
                if (y != height - 1)//no linebreak after the last row
                    Console.WriteLine();
            }
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
    //some classes - or put them in other files

    class Hero
    {
        public static int positionY;
        public static int positionX;

        public static string status;

        public static int step;

        public static string direction;


        public static string at;
        public static string below;
        public static string above;
        public static string leftSide;
        public static string rightSide;

    }

    class Level
    {

        public static int startY = 18;
        public static int startX = 4;

        public static string[] map = {
                                        "                                ",
                                        "  HZZZ     ZZZ               D  ",
                                        "  H   ZZZZZ   ZZ       Z Z ZZZ  ",
                                        "  H             ZZZZZZ          ",
                                        "  ZZHZZ   ZZZ                   ",
                                        "    H  ZZZ             ZZZHZZ   ",
                                        "    H           ZZ        H     ",
                                        "    H        ZZH          H     ",
                                        "    H       Z  H          H     ",
                                        "    H      Z   H   ZZ    ZZZZH  ",
                                        "    ZZ ZZZZ    H  Z   ZZ     H  ",
                                        "               ZZZ           H  ",
                                        "  ZZZ                        H  ",
                                        "      ZHZZ        HZZ        H  ",
                                        "       H  Z       H        ZZZ  ",
                                        "       H   ZZZZH ZZ   ZZ H      ",
                                        "       H       H         H      ",
                                        "       H       H       ZZZ      ",
                                        "       H       ZZ   ZZZ         ",
                                        "  ZZZZZZZ        ZZZ            ",
                                        "                                "
                                    };

    }

    class Debug
    {
        public static int counter;
    }
}
